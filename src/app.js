const path = require("path");
const express = require("express");
const app = express();
const morgan = require("morgan");
const mongoose = require("mongoose");
const cors = require('cors')
// import cors from "cors";

//connecting to db
mongoose
  .connect("mongodb://localhost/mini-juegos")
  .then(db => console.log("db connected"))
  .catch(err => console.log(err));

//importing routes
//ej: import indexRoutes from './routes'
const hangedRoutes = require("./routes/hanged");

//settings
app.use(cors());
app.set("port", process.env.PORT || 5000);
// app.set("views", path.join(__dirname, "views"));
// app.set("view engine", "ejs");

//middlewares
app.use(morgan("dev"));
app.use(express.urlencoded({ extended: false }));

//routes
app.use("/ahorcado", hangedRoutes);

// starting the server
app.listen(app.get("port"), () => {
  console.log(`escuchando puerto http://localhost:${app.get("port")}`);
});
