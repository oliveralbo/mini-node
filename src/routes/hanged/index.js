const express = require("express");
const router = express.Router();
// const Words = require("../models/hanged.js");
const Words = require("../../models/hanged.js");

router.get("/", async (req, res) => {
  const words = await Words.find();
  random = Math.floor(Math.random() * words.length);
  res.send(words[random]);
});

router.post("/add", async (req, res) => {
  const words = new Words(req.body);
  try {
    await words.save();
    res.send("se grabo correctamente");
    // res.redirect("/");
  } catch {
    res.send("fallo la carga d datos. vuelva a intentarlo mas tarde!!");
  }
  // console.log(req);
});

router.get("/delete/:id", async (req, res) => {
  const { id } = req.params;
  await Words.remove({ _id: id });
  res.send("se borro correctamente");
  // res.redirect("/");
});

router.get("/turn/:id", async (req, res) => {
  const { id } = req.params;
  const words = await Words.findById(id);
  words.status = !words.status;
  await words.save();
  res.redirect("/");
});

router.get("/edit/:id", async (req, res) => {
  const { id } = req.params;
  const words = await Words.findById(id);

  res.render("edit", { words });
});

router.post("/edit/:id", async (req, res) => {
  const { id } = req.params;
  await words.update({ _id: id }, req.body);

  res.redirect("/");
});

module.exports = router;
